<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Model;
use AppBundle\Entity\Vehicle;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="form")
     */
    public function indexAction(Request $request)
    {
        $defaultData = ['message' => 'Form many to one'];
        $form = $this->createFormBuilder($defaultData)
            ->add('NameModel', TextType::class)
            ->add('CostModel', IntegerType::class)
            ->add('ProducerModel', TextType::class)
            ->add('FirstVehicleName', TextType::class)
            ->add('FirstVehicleWheel', IntegerType::class)
            ->add('FirstVehicleSize', TextType::class)
            ->add('SecondVehicleName', TextType::class)
            ->add('SecondVehicleWheel', IntegerType::class)
            ->add('SecondVehicleSize', TextType::class)
            ->add('send', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $model = new Model();
            $model->setName($form->get('NameModel')->getData());
            $model->setCost($form->get('CostModel')->getData());
            $model->setProducer($form->get('ProducerModel')->getData());

            $vehicle = new Vehicle();
            $vehicle->setName($form->get('FirstVehicleName')->getData());
            $vehicle->setWheel($form->get('FirstVehicleWheel')->getData());
            $vehicle->setSize($form->get('FirstVehicleSize')->getData());

            $vehicle2 = new Vehicle();
            $vehicle2->setName($form->get('SecondVehicleName')->getData());
            $vehicle2->setWheel($form->get('SecondVehicleWheel')->getData());
            $vehicle2->setSize($form->get('SecondVehicleSize')->getData());


            $vehicle->setModel($model);
            $vehicle2->setModel($model);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($model);
            $entityManager->persist($vehicle);
            $entityManager->persist($vehicle2);
            $entityManager->flush();

            return $this->render('info.html.twig');

        }

        return $this->render('default/index.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
